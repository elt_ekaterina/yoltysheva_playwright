const assert = require('assert');
import playwright from 'playwright';

describe ('UI Demo suit', () =>{
    let browser, context, page;
    beforeEach( async () => {
        browser = await playwright.chromium.launch({
            headless: false,
            slowMo: 250,
            });
        context = await browser.newContext();
        page = await context.newPage();
        await page.goto('https://try.vikunja.io/')
        })
    afterEach( async () => {
        await page.screenshot({path: 'screenshot.png'});
        await page.close();
        await browser.close();
        })

    it('UI test', async () => {
        await page.click('#username');
        await page.fill('#username', 'demo');
        await page.click('#password');
        await page.fill('#password', 'demo');
        await page.click('.is-primary');
        const profileName = ('.user > .dropdown > .dropdown-trigger > .button > .username')
        const profileNameText = await page.textContent(profileName)
        assert.deepEqual(profileNameText, 'demo');
    })
})
